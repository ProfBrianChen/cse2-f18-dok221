//////// CSE HW 001
////

public class WelcomeClass{
  
  public static void main (String args []){
    // prints the 9 lines below
    System.out.println ("  -----------");
    System.out.println ("  | WELCOME |");
    System.out.println ("  -----------");
    System.out.println ("  ^ ^ ^ ^ ^ ^");
    System.out.println (" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println ("<-D--O--K--2--2--1->");
    System.out.println (" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println ("  v v v v v v");
    System.out.println (" I love Cold Brew.");
    
  }
  
}
