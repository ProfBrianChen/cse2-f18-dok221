//HW 02 for arithmetic
public class Arithmetic {
  
  public static void main (String args[]){
    // Setting up initial values
    int numPants = 3; // # of pairs of pants
    double pantsPrice = 34.98; // cost per pair of pants
    
    int numShirts = 2; // # of sweatshirts
    double shirtPrice = 24.99; // cost per sweatshirts
    
    int numBelts = 1; // # of belts
    double beltPrice = 33.99; // cost per belt
    
    double paSalesTax = 0.06; // tax rate
    
    // Declaring variables
    double totalCostOfPants; // total cost of pants
    double totalCostOfShirts; // total cost of shirts
    double totalCostOfBelts; // total cost of belts
    
    double salesTaxOnPants; // sales tax charged on pants
    double salesTaxOnShirts; // sales tax charged on shirts
    double salesTaxOnBelts; // sales tax charged on belts
    double totalCostBeforeTax; // total cost of purchases (before tax)
    double totalSalesTax; // total sales tax
    double totalCost; // total paid for this transaction, including sales tax
    
    //Rounded up values
    double cleanSalesTaxOnPants;
    double cleanSalesTaxOnShirts;
    double cleanSalesTaxOnBelts;
    double cleanTotalSalesTax;
    double cleanTotalCost;
    
    //Calculation of Declared Variables
    totalCostOfPants = numPants * pantsPrice;
    totalCostOfShirts = numShirts * shirtPrice;
    totalCostOfBelts = numBelts * beltPrice;
    
    salesTaxOnPants = totalCostOfPants * paSalesTax;
    salesTaxOnShirts = totalCostOfShirts * paSalesTax;
    salesTaxOnBelts = totalCostOfBelts * paSalesTax;
    
    totalCostBeforeTax = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;
    totalSalesTax = salesTaxOnPants + salesTaxOnShirts + salesTaxOnBelts;
    totalCost = totalCostBeforeTax + totalSalesTax;
    
    cleanSalesTaxOnPants = Math.round(salesTaxOnPants * 100.0) / 100.0;
    cleanSalesTaxOnShirts = Math.round(salesTaxOnShirts * 100.0) / 100.0;
    cleanSalesTaxOnBelts = Math.round(salesTaxOnBelts * 100.0) / 100.0;
    cleanTotalSalesTax = Math.round(totalSalesTax * 100.0) / 100.0;
    cleanTotalCost = Math.round(totalCost * 100.0)/ 100.0;
    
    System.out.println ("Total Cost of Pants: " + totalCostOfPants);
    System.out.println ("Total Cost of Shirts: " + totalCostOfShirts);
    System.out.println ("Total Cost of Belts: " + totalCostOfBelts);
    System.out.println ("Sales Tax on Pants: " + cleanSalesTaxOnPants);
    System.out.println ("Sales Tax on Shirts: " + cleanSalesTaxOnShirts);
    System.out.println ("Sales Tax on Belts: " + cleanSalesTaxOnBelts);
    System.out.println ("Total Cost Before Taxes: " + totalCostBeforeTax);
    System.out.println ("Total Sales Tax: " + cleanTotalSalesTax);
    System.out.println ("Total Cost: " + cleanTotalCost);
    
  }
}