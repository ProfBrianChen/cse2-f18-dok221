/*#1 job file 

CSE 002 class
Dongmin Kim
Sep 17, 2018
HW 03
*/

//Necessary for scan
import java.util.Scanner;

//Necessary for every single code
public class Convert {
  
  public static void main (String args[]){
    Scanner myScanner = new Scanner (System.in); // to scan
      
    //Declaring variables and scan input
    System.out.print ("Enter the affected areas in acres: "); 
    double areaAffected = myScanner.nextDouble (); // makes you able to input data to scan
    
    System.out.print ("Enter the rainfall in affected area: "); 
    double rainFall = myScanner.nextDouble (); // makes you able to input data to scan
    
    //Formulas
    double gallons;
    gallons = areaAffected * rainFall * 27154.285714286; //conversion to gallons
    
    double cubicMiles;
    cubicMiles = gallons * 	9.0816859724455 * Math.pow (10, -13); //conversion to cubicMiles
    cubicMiles = Math.round (cubicMiles * 100000000.0)/100000000.0; //Round up to 8 decimals as shown in instruction
    
    System.out.println (cubicMiles + " cubic miles"); // final printout to the screen
      
  }
}