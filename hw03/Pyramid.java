/*#2 job file 

CSE 002 class
Dongmin Kim
Sep 17, 2018
HW 03
*/

//Necessary for scan
import java.util.Scanner;

//Necessary for every single code
public class Pyramid {
  
  public static void main (String args[]){
    Scanner myScanner = new Scanner (System.in); // to scan
      
    //Declaring variables and scan input
    System.out.print ("The square side of the pyramid is (input length): ");
    int squareSideLength = myScanner.nextInt (); // makes you able to input data to scan  
      
    System.out.print ("The height of the pyramid is (input height): ");
    int heightPyramid = myScanner.nextInt (); // makes you able to input data to scan  
    
    //Formulas
    int volumePyramid;
    volumePyramid = (squareSideLength * squareSideLength * heightPyramid) / 3; //formula for volume
    
    System.out.println ("The volume inside the pyramid is: " + volumePyramid + "."); //final print out to the screen
    
  }
}