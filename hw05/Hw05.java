/* Daniel's Lab 05 Hw05.java

CSE 002 class
Dongmin Kim
Oct 9, 2018 
HW 05
*/

import java.util.Random; //to create random numbers
import java.util.Scanner; //to be able to scan

public class Hw05{
  //main method required for every Java program
  public static void main (String args[]){
    Random randGen = new Random ();
    Scanner myScanner = new Scanner (System.in); // Required for scan
    
    int number1; //declared it
    number1 = randGen.nextInt (52) + 1; //from 1 to 52 INSTEAD of 0 to 51
    
    int number2; //declared it
    number2 = randGen.nextInt (52) + 1; //from 1 to 52 INSTEAD of 0 to 51
    
    int number3; //declared it
    number3 = randGen.nextInt (52) + 1; //from 1 to 52 INSTEAD of 0 to 51
    
    int number4; //declared it
    number4 = randGen.nextInt (52) + 1; //from 1 to 52 INSTEAD of 0 to 51
    
    int number5; //declared it
    number5 = randGen.nextInt (52) + 1; //from 1 to 52 INSTEAD of 0 to 51
    
    // Here we select what suit it is
    if (number >= 1 && number <= 13) {
      suit = "diamonds";
    }
    
    if (number >= 14 && number <= 26) {
      suit = "clubs";
    }
    
    if (number >= 27 && number <= 39) {
      suit = "hearts";
    }
    
    if (number >= 40 && number <= 52) {
      suit = "spades";
    }
    
    int numberOfTries; //how many times you tried
    numberOfTries = myScanner.nextInt();
    
    int remainder1;
    remainder1 = number1 % 13;
    
    int remainder2;
    remainder2 = number2 % 13;
    
    int remainder3;
    remainder3 = number3 % 13;
    
    int remainder4;
    remainder4 = number4 % 13;
    
    int remainder5
    remainder5 = number5 % 13;
    
    String "4-kind";
    String "3-kind";
    String "2-pair";
    String "1-pair";
    
    if (remainder1 == remainder2 == remainder3 == remainder4 != remainder5) {
       "4-kind";
    }
    
    if (remainder1 == remainder2 == remainder3 == remainder5 != remainder4) {
       "4-kind";
    }
    
    if (remainder1 == remainder2 == remainder4 == remainder5 != remainder3) {
       "4-kind";
    }
    
    if (remainder1 == remainder3 == remainder4 == remainder5 != remainder2) {
       "4-kind";
    }
    
    if (remainder2 == remainder3 == remainder4 == remainder5 != remainder1) {
       "4-kind";
    }
    
    