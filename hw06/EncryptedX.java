/* Daniel's HW06 

CSE 002 class
Dongmin Kim
Oct 23, 2018 
HW 06
*/

import java.util.Scanner; // necessary

public class EncryptedX{
  //main method required for every Java program
  public static void main (String args[]){
    Scanner myScanner = new Scanner(System.in); //for scan
    
    //Checking if it is an integer
    System.out.println ("Type a number that is in between 0 and 100");
    boolean user_input = myScanner.hasNextInt();
    while (user_input == false) {
      String junk = myScanner.next();
      System.out.println ("Type AN INTEGER that is in between 0 and 100");
      user_input = myScanner.hasNextInt();
    }
    
    //Checking if it is between 0 and 100
    int newInput = myScanner.nextInt();
    while (newInput < 0 || newInput > 100) {
      System.out.println ("Type an integer WITHIN THE VALUE OF 0 and 100");
      newInput = myScanner.nextInt();
    }
      
    //Let's go for the Patterns!
    for (int i = 0; i<= newInput; ++i) {
      for (int j = 0; j <= newInput; ++j) {
        if (j == i || j == newInput-i) {
          System.out.print (" ");
        }
        else {
        System.out.print("*");
        }
      }
      System.out.println ("");
    }
  }
}