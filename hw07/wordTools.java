/*Daniel Kim
CSE 002 
Oct 31, 2018
HW 07
*/

import java.util.Scanner;

public class wordTools {
  
  public static String sampleText (String text) {
    return text;
  }
  
  
  public static void printMenu () {
    System.out.println ("");
    System.out.println ("MENU\nc - Number of non-whitespace characters\nw - Number of words\nf - Find text\nr - Replace all !'s\ns - Shorten spaces\nq - Quit");  
    System.out.println ("Choose an option: ");
  }
  
  //java methods
  public static int getNumOfNonWSCharacters (String text) {
    int counter = 0;
    
    for (int i = 0; i <= text.length () -1; i++) {
      if (text.charAt (i) != ' ') {
        counter ++;
      }
    }
    return counter;
  }
  
  //java methods
  public static int getNumOfWords (String text) {
     int counter = 0;
     
     String wordCount[] = text.split (" ");
     counter = wordCount.length;
     
     return counter;
   }
  
   //java methods
   public static int findText (String text, String word) {
     int counter = 0;
     String words[] = text.split(" "); 

      for (int i = 0; i < words.length; i++)  { 
        
        if (words[i].equals(word)) {
          counter++; 
        }
      } 

      return counter; 
   } 

   //java methods
   public static void replaceExclamation (String text) {
     String newString = text.replace('!', '.');
     System.out.print (newString);
   }
   
   //java methods
   public static void shortenSpace (String text) {
     text = text.trim().replaceAll(" +", " ");
     System.out.print (text);
   }
   
   //Main Method
   public static void main (String [] args) {
    Scanner scnr = new Scanner (System.in);
    boolean open = true;
    
    System.out.println ("Enter a sample text:");
    String text = scnr.nextLine();
    
    System.out.println (sampleText (text));
    while (open = true) {
    printMenu();
    
    String option = scnr.next();
    if (option.equals ("c")) {
      System.out.println (getNumOfNonWSCharacters (text));
    }
    
    if (option.equals ("w")) {
      System.out.println (getNumOfWords (text));
    }
    
    if (option.equals ("f")) {
      System.out.println ("Enter a word or phrase to be found: ");
      scnr.nextLine();
      String word = scnr.nextLine();
      System.out.println ("\"" + word + "\" "+ "instances: " +findText (text,word));
    }
    
    if (option.equals ("r")) {
      System.out.println ("Edited text: ");
      replaceExclamation(text);
    } 
    
    if (option.equals ("s")) {
      System.out.println ("Edited text: ");
      shortenSpace(text);
    } 
    
    if (option.equals ("q")) {
      open = false;
      break;
    } 
  } 
 }
}