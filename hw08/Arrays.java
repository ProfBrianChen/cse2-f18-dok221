import java.util.Scanner;
import java.util.Random;

public class Arrays{
  
  public static void printArray (String [] list) {
    for (int i =  0; i < list.length; i++) {
      System.out.print(list [i] + " ");
    }
  }
  
  public static void shuffle (String [] list) {
    Random rGen = new Random ();
    int num = rGen.nextInt (51);
    String temp = "";
    System.out.println("\nShuffled");
    for (int i = 0; i < 52; i++) {
      temp = list[0];
      list[0] = temp;
      list[num] = temp;
    }
  }  
  
  public static String [] getHand (String [] list, int index, int numCards) {
    String[] hand = new String [numCards];
    for (int i = list.length - 1; i >= list.length - numCards; i--){
      hand[list.length - i - 1] = list[i];
    }
    System.out.println("\nHand");
    return hand;   
  }
  
  public static void main (String [] args) { 
    Scanner scan = new Scanner(System.in);
//suits club, heart, spade or diamond
    
    String[] suitNames={"C","H","S","D"};
    String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
    String[] cards = new String[52];
    String[] hand = new String[5];

    int numCards = 5;
    int again = 1;
    int index = 51;
    
    for (int i = 0; i < 52; i++){
      cards[i] = rankNames [i%13] + suitNames [i/13];
      System.out.print(cards[i] + " "); 
    }
    
    System.out.println(); 
    printArray (cards); 
    shuffle (cards); 
    printArray (cards); 
    
    while (again == 1) {
      hand = getHand(cards,index,numCards);
      printArray (hand);
      index = index-numCards;
      System.out.println ("\nEnter a 1 if you want another hand drawn"); 
      again = scan.nextInt();
    } 
  }
}