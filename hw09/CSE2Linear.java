/* Daniel's HW09.java

CSE 002 class
Dongmin Kim
Nov 27, 2018 
HW 09
*/

import java.util.Scanner;
import java.util.Random;

public class CSE2Linear{
  
  public static void linearSearch(int exams[], int number) { 
    boolean correct = false;
    
    int count = 0;
    for (int i = 0; i < exams.length; i++) { 
      if (exams[i] == number) {
        System.out.println (number + " was found in the list with " + count + " iterations");
        correct = true;
        break;
      }
    count ++;      
    } 
    if (correct == false) {
      System.out.println (number + " was not found in the list with " + count + " iterations");
    }
    
  } 
 
  public static void randomScrambling(int[] array){
    Random rand = new Random();  // Random number generator   
    for (int i=0; i<array.length; i++) {
      int randomPosition = rand.nextInt(array.length);
      int temp = array[i];
      array[i] = array[randomPosition];
      array[randomPosition] = temp;
    }
  }
  
  public static void binarySearch(int numbers [], int numbersSize, int key) {
      int mid;
      int low;
      int high;
      int count = 0;
      boolean correct = false;
      
      low = 0;
      high = numbersSize - 1;

      while (high >= low) {
         mid = (high + low) / 2;
         if (numbers[mid] < key) {
            low = mid + 1;
         } 
         else if (numbers[mid] > key) {
            high = mid - 1;
         } 
         else {
            System.out.println (key + " was found in the list with " + count + " iterations");
            correct = true;
            break;
         }
         count ++;
      }

      if (correct == false) {
        System.out.println (key + " was not found in the list with " + count + " iterations");
      }
  }
  
  //main method required for every Java program
  public static void main (String args[]){
    Scanner scan = new Scanner (System.in);
    int[]exams = new int [15];
    
   System.out.println ("Enter 15 ascending ints for final grades in CSE2: ");
    
    for (int i = 0; i < 15; i++) {
      boolean correctInt = false;
      
      while (!correctInt) {
        correctInt = scan.hasNextInt();
        if (correctInt == true) {
          exams[i] = scan.nextInt();
        }
        
        else {
          System.out.println ("Type an integer");
          scan.next();
        }
        
        if (exams[i] > 100 || exams[i] < 0) {
          System.out.println ("It is not within the range. PLEASE re-type.");
          correctInt = false;
        }
        
        if (i > 0 && exams[i] < exams[i-1]) {
          System.out.println ("The # has to be greater than the last one. PLEASE re-type.");
          correctInt = false;
        }
      }
    }
    
    for (int i = 0; i < 15; i++) {
      System.out.print (exams[i] + " ");
    }    
    
    System.out.println ("\nEnter a grade to search for: ");
    
    int number = 0;
    boolean correctInt = false;
    while (!correctInt) {
      correctInt = scan.hasNextInt();
      if (correctInt == true) {
        number = scan.nextInt();
      }
        
      else {
        System.out.println ("Type an integer");
        scan.next();
      }
        
      if (number > 100 || number < 0) {
        System.out.println ("It is not within the range. PLEASE re-type.");
        correctInt = false;
      }
    }
    
    CSE2Linear.binarySearch(exams, exams.length, number); 
    
    System.out.println ("Scrambled: ");
    CSE2Linear.randomScrambling(exams);
    
    for (int i = 0; i < 15; i++) {
      System.out.print (exams[i] + " ");
    }
      
    System.out.println ("\nEnter a grade to search for: ");
    
    number = 0;
    correctInt = false;
    while (!correctInt) {
      correctInt = scan.hasNextInt();
      if (correctInt == true) {
        number = scan.nextInt();
      }
        
      else {
        System.out.println ("Type an integer");
        scan.next();
      }
        
      if (number > 100 || number < 0) {
        System.out.println ("It is not within the range. PLEASE re-type.");
        correctInt = false;
      }
    }
    
    CSE2Linear.linearSearch(exams, number);
  }
}