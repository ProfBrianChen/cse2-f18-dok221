/* Daniel's HW09.java

CSE 002 class
Dongmin Kim
Nov 27, 2018 
HW 09
*/

import java.util.Scanner; //scan method
import java.util.Random; //random method

public class RemoveElements{
  public static int[] randomInput () {
    Random rand = new Random ();
    
    int[]array = new int [10]; //making a new array
    for (int i = 0; i < 10; i++) {
      array[i] = (int) (Math. random() * 9 + 1);
    }
    
    return array;
  }
  
  public static int[] delete(int list[], int pos) {
    int []array = new int [list.length-1];
    
    if (pos > list.length-1 || pos < 0) {
      System.out.println ("The index is not valid."); //printing out it is not valid
      System.out.print ("The output array is ");
                        
     for (int i = 0; i < list.length; i++) {
      System.out.print (list[i] + " ");
    }
    
    System.out.println ("");
      return list;
    }
    
    for (int i = 0; i < array.length; i++) {
      if (i != pos) {
        if (i > pos) {
          array[i] = list[i+1];
        }
        else {
          array[i] = list[i];
        }
      }
    }
    
    System.out.print ("The output array is "); //printing out what the output array is
                        
    for (int i = 0; i < array.length; i++) {
      System.out.print (array[i] + " ");
    }
    
    System.out.println ("");
    
    return array;
  }
  
  public static int[] remove(int list[], int target) {
    boolean correct = false;
    
    int []array = new int [list.length-1];
    
    for (int i = 0; i < list.length; i++) {
      if (target == list[i]) {
        correct = true;
        
        for (int j = 0; j < array.length; j++) {
          if (j != i) {
            if (j > i) {
              array[j] = list[j+1];
            }
            else {
              array[j] = list[j];
            }
          }
        }
      }
    }
    
    if (correct == false) {
      System.out.println ("Element " + target + " was not found");
      
      System.out.print ("The output array is ");
                        
      for (int i = 0; i < list.length; i++) {
        System.out.print (list[i] + " ");
      }
    
      System.out.println ("");
      return list;
    }
    
    else {
      System.out.println("Element " + target + " has been found");
      System.out.print ("The output array is ");
                        
      for (int i = 0; i < array.length; i++) {
        System.out.print (array[i] + " ");
      }
    
      System.out.println ("");
      return array;
    }
    
  }
  
  
  public static void main(String [] arg) { 
    Scanner scan = new Scanner(System.in);
    int num[] = new int[10]; 
    int newArray1[];
    int newArray2[];
    int index, target;
    
    String answer="";
    
    do{
      System.out.print("Random input 10 ints [0-9]"); 
      num = randomInput();
      String out = "";
      out += listArray(num); 
      
      System.out.println(out);
      System.out.print("Enter the index ");
      
      index = scan.nextInt();
      newArray1 = delete(num,index);
      
      String out1="The output array is ";
      out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}" System.out.println(out1);
      
      System.out.print("Enter the target value ");
      target = scan.nextInt();
      newArray2 = remove(num,target);
      
      String out2="The output array is ";
      out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}" System.out.println(out2);
      
      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-"); 
      answer=scan.next();
    }
    
    while(answer.equals("Y") || answer.equals("y"));
  
  }

  public static String listArray(int num[]){ String out="{";
    for (int j = 0; j < num.length; j++){ 
      if (j> 0){
        out+=", ";
      }
      out+=num[j]; 
    }
    
    out+="} "; 
    return out;
} }