/* 
Dongmin Kim
Sep 07, 2018
CSE 002

My bicycle cyclometer (meant to measure speed, distance, etc.) 
Records two kinds of data
a. the time elapsed in seconds
b. and the number of rotations of the front wheel during that time

For two trips, given time and rotation count, your program should print the number of minutes for each trip
1. print the number of counts for each trip
2. print the distance of each trip in miles
3. print the distance for the two trips combined
*/

public class Cyclometer {
  //main method required for every Java program
  public static void main (String args[]){
    //Data input
    int secsTrip1 = 480; //secsTrip 1 data
    int secsTrip2 = 3220; //secsTrip 2 data
    int countsTrip1 = 1561; //# of counts (rotation) for Trip 1
    int countsTrip2 = 9037; //# of counts (rotation) for Trip 2
    
    //Constants (Units) & Storing
    double wheelDiameter = 27.0; // wheelDiameter is 27
    double PI = 3.14159; // value of PI is 3.14159
    double feetPerMile = 5280; // There is 5280 feet in a mile
    double inchesPerFoot = 12; // There are 12 inches in a feet
    double secondsPerMinute = 60; //There are 60 seconds per minute
    double distanceTrip1, distanceTrip2, totalDistance; //storage value
    
    //Print out that Trip 1 (or 2) took x mins and had x counts.
    System.out.println ("Trip 1 took " + (secsTrip1/secondsPerMinute) +" minutes and had "+ countsTrip1 +" counts."); 
    System.out.println ("Trip 2 took " + (secsTrip2/secondsPerMinute) +" minutes and had "+ countsTrip2 +" counts.");
      
    //Now calculation starts.
    distanceTrip1 = countsTrip1 * wheelDiameter * PI; // Gives distances in inches
    distanceTrip1/= inchesPerFoot * feetPerMile; // Gives (converts) distance in miles
    distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile; //Same thing as line 39 ~ 40 but combined it
    totalDistance = distanceTrip1 + distanceTrip2; // Added 2 distances to get the total distance
    
    //Print out the output data
    System.out.println ("Trip 1 was "+ distanceTrip1 +" miles"); // Printing out info about Trip 1
    System.out.println ("Trip 2 was "+ distanceTrip2 +" miles"); // Printing out info about Trip 2
    System.out.println ("The total distance was "+ totalDistance +" miles"); // Printing out info about Total distance
  }
}