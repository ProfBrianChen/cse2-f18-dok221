/* Daniel's Lab 03 Check.java

CSE 002 class
Dongmin Kim
Sep 14, 2018
Lab 3
*/

// Necessary for scan
import java.util.Scanner;

//Necessary for every single code
public class Check{
 
  public static void main (String[] args){
    Scanner myScanner = new Scanner (System.in); // Required for scan
    
    //Scan and print
    System.out.print ("Enter the original cost of the check in the form xx.xx: ");
    double checkCost = myScanner.nextDouble(); // makes you available to input data for scan
    
    System.out.print ("Enter the percentage tip that you wish to pay as a whole number (in the form xx) : ");
    double tipPercent = myScanner.nextDouble(); // makes you available to input data for scan
    
    tipPercent /= 100; //Converting percentage to decimal value
    
    System.out.print ("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt();
    
    //Printing out data and variables
    // Assigning variables
    double totalCost;
    double costPerPerson;
    int dollars; // whole dollar amount of cost
    int dimes, pennies; // storing digits to the right of the decimal point for the cost 
    
    // Formulas for Variables
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople;
    dollars = (int) costPerPerson; // Receive the whole $ value
    dimes = (int) (costPerPerson * 10) % 10; // Getting dimes amount
    pennies = (int) (costPerPerson * 100) % 10; // Getting pennies amount
    
    System.out.println ("Each person in the group owes $ " + dollars + '.' + dimes + pennies); // Final printing out
    
  } // end of main method
} // end of class