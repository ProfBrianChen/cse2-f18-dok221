/* Daniel's Lab 04 CardGenerator.java

CSE 002 class
Dongmin Kim
Sep 21, 2018 (Sick so delayed)
Lab 4
*/

import java.util.Random; //to create random numbers

public class CardGenerator{
  //main method required for every Java program
  public static void main (String args[]){
    Random randGen = new Random ();
  
    int number; //declared it
    number = randGen.nextInt (52) + 1; //from 1 to 52 INSTEAD of 0 to 51
    
    int remainder;
    remainder = number % 13;
    
    String suit;
    String identity;
    
    suit = " ";
    identity = " ";
    
    // Here we select what suit it is
    if (number >= 1 && number <= 13) {
      suit = "diamonds";
    }
    
    if (number >= 14 && number <= 26) {
      suit = "clubs";
    }
    
    if (number >= 27 && number <= 39) {
      suit = "hearts";
    }
    
    if (number >= 40 && number <= 52) {
      suit = "spades";
    }
    
    switch (remainder) {
      case 0:
        identity = "King";
        break;
      
      case 1:
        identity = "Ace";
        break;
        
      case 11:
        identity = "Jack";
        break;
        
      case 12:
        identity = "Queen";
        break;
      
      default:
        identity = "" + remainder; //Prints remainder <Changes integer to a string>
        break;
    }
      
    System.out.println ("You picked the " + identity + " of " + suit);
  
  }
}
