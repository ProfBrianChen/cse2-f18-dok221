/* Daniel's Lab 06 

CSE 002 class
Dongmin Kim
Oct 19, 2018 
Lab 6
*/

import java.util.Scanner; // necessary

public class PatternB{
  //main method required for every Java program
  public static void main (String args[]){
    Scanner myScanner = new Scanner(System.in); //for scan
    
    //Checking if it is an integer
    System.out.println ("Type a number that is in between 0 and 10");
    boolean user_input = myScanner.hasNextInt();
    while (user_input == false) {
      String junk = myScanner.next();
      System.out.println ("Type AN INTEGER that is in between 0 and 10");
      user_input = myScanner.hasNextInt();
    }
    
    //Checking if it is between 1 and 10
    int newInput = myScanner.nextInt();
    while (newInput < 1 || newInput > 10) {
      System.out.println ("Type an integer WITHIN THE VALUE OF 0 and 10");
      newInput = myScanner.nextInt();
    }
    
    //Pattern B
    for (int i = newInput; i >= 1; --i) {
      for (int j = 1; j <= i; ++j) {
        System.out.print (j);
      }
      System.out.println ("");
    }
  }
}