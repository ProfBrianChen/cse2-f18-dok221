import java.util.Random;

public class sentenceGenerator {
  
  public static String Adjective (int randomInt) {
    switch (randomInt) {
      case 0:
        return "fluffy";
      
        
      case 1:
        return "pretty";
        
        
      case 2:
        return "handsome";
        
        
      case 3:
        return "annoying";
        
        
      case 4:
        return "disgusting";
        
        
      case 5:
        return "funny";
      
        
      case 6:
        return "nice";
       
        
      case 7:
        return "generous";
       
        
      case 8:
        return "pretty";
        
      case 9:
        return "cool";
        
      default:
        return "";
    }
  }
  
  public static String Noun (int randomInt) {
    switch (randomInt) {
      case 0:
        return "dog";
        
        
      case 1:
        return "cat";
        
        
      case 2:
        return "hamster";
      
        
      case 3:
        return "pineapple";
       
        
      case 4:
        return "tree";

        
      case 5:
        return "human";
        
        
      case 6:
        return "computer";
        
        
      case 7:
        return "lamp";
      
        
      case 8:
        return "moth";
       
        
      case 9:
        return "cow";
        
      default:
        return "";
        
    }
  }
  
  public static String Verb (int randomInt) {
    switch (randomInt) {
      case 0:
        return "ate";
       

      case 1:
        return "ran";
        
        
      case 2:
        return "swam";
        
        
      case 3:
        return "drank";
        
        
      case 4:
        return "vomited";
        
        
      case 5:
        return "kissed";
        
        
      case 6:
        return "kicked";
        
        
      case 7:
        return "slept";
        
        
      case 8:
        return "bullied";
       
        
      case 9:
        return "died";
        
      default:
        return "";
    }
  }
  
  public static void main (String []args) {
    
    Random randomGenerator = new Random ();
    int randomInt = randomGenerator.nextInt(10);
    
    //Phase 1
    System.out.print ("The "); 
    
    randomInt = randomGenerator.nextInt(10);    
    System.out.print (Adjective(randomInt)); 
    
    randomInt = randomGenerator.nextInt(10);    
    System.out.print (" " +Adjective(randomInt)); 
    
    randomInt = randomGenerator.nextInt(10);    
    System.out.print (" " +Noun(randomInt)); 
    
    randomInt = randomGenerator.nextInt(10); 
    System.out.print (" " +Verb(randomInt));
    
    System.out.print (" the "); 
  
    randomInt = randomGenerator.nextInt(10);    
    System.out.print (Adjective(randomInt)); 
    
    randomInt = randomGenerator.nextInt(10);    
    System.out.print (" " +Noun(randomInt)); 
    
    //Phase 2
    for (int i = 0; i < 7; i++) {
        System.out.println ("");
      
        System.out.print ("The "); 
    
        randomInt = randomGenerator.nextInt(10);    
        System.out.print (Adjective(randomInt)); 
    
        randomInt = randomGenerator.nextInt(10);    
        System.out.print (" " +Adjective(randomInt)); 
    
        randomInt = randomGenerator.nextInt(10);    
        System.out.print (" " +Noun(randomInt)); 

        randomInt = randomGenerator.nextInt(10); 
        System.out.print (" " +Verb(randomInt));

        System.out.print (" the "); 

        randomInt = randomGenerator.nextInt(10);    
        System.out.print (Adjective(randomInt)); 

        randomInt = randomGenerator.nextInt(10);    
        System.out.print (" " +Noun(randomInt)); 
    }
    
  }
}