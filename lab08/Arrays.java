/* Daniel's Lab 08.java

CSE 002 class
Dongmin Kim
Nov 14, 2018 
Lab 08
*/

import java.util.Random; //to create random numbers

public class Arrays{
  //main method required for every Java program
  public static void main (String args[]){
    Random randGen = new Random ();
    
    int[] num1 = new int[100];
    int[] num2 = new int[100];
    
    for (int i = 0; i < num1.length; i++) {
      num1[i] = (int) randGen.nextInt (100);
    }
    
    for (int i = 0; i < num2.length; i++) {
      num2[num1[i]] += 1;
    }
    
    String a = "Array 1 holds the following integers: ";
    
    for (int i = 0; i < num2.length; i++) {
      a += num1[i] + " "; 
     }
    
    System.out.println (a);
    System.out.println ();
                          
    for (int i = 0; i < num2.length; i++) {
      if (num1[i] > 1) {
        System.out.println (i + " occurs " + num2[i] + " times ");
      }
      else {
        System.out.println (i + " occurs " + num2[i] + " time ");
      }
    }
  }
}
    